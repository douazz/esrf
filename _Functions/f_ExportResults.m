%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% DiNi(2022) %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%% Function that Export Results in .Res File in C:// %%%%%%%%%
%%%%%%%%%%%%%%%%%% M.Spitoni - Created on 09/02/2023 %%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%input: appData as app.Data,  pathexport is a pathfile

function f_ExportResults(appData,pathexport)
    writetable(appData, pathexport,"FileType","text","Delimiter","\t","WriteVariableNames",false,...
                    "WriteRowNames",false);
    disp("File updated correly in C://");
end

